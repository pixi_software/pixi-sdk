pixi* SDK
========

Framework for rapid application development for pixi* ecosystem.

SDK dependecies
===============

   * [**pixi/ui**][3] - Ui library for pixi application based on {wrap}bootstrap [ace] template
   * [**pixi/api**][4] - SOAP Client library 
   * [**pixi/factory**][5] - Library to initialise the soap Client in SDK
   * [**components/pixi**][6] - Assets of the SDK framework.

   
Installing the SDK framework
============================

### Installation requirements 
   * [**php**][9] >= 5.6
   * [**Composer**][12]

#### Install the SDK by downloading the sources
You can download the sources of the SDK from the downloads area in the menu on the left side. Here you can download the sources of a specific branch or a tagged version.

If you download the repository you need to, install the development dependencies via composer. From the root run:  
  
 $ php composer install  

Now you're good to go. Just go to your browser and navigate to your project.

[3]: https://bitbucket.org/pixi_software/pixi-sdk-ui
[4]: https://bitbucket.org/pixi_software/pixi-sdk-soap
[5]: https://bitbucket.org/pixi_software/pixi-sdk-factory
[6]: https://bitbucket.org/pixi_software/pixi-sdk-assets
[9]: http://php.net/downloads.php
[12]: https://getcomposer.org/